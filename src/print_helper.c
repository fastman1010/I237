#include <avr/pgmspace.h>
#include "hmi_msg.h"
#include "print_helper.h"
#include "andygock_avr-uart/uart.h"

/* Print only text characters of ASCII chart (whitespace to tilde) */
void print_ascii_tbl(void)
{
    for (char c = ' '; c <= '~'; c++)
    {
        uart0_putc(c);
    }

    uart0_puts_p(crlf); /* Insert CR+LF */
}

/* Print given array of characters in human-readable form */
void print_for_human(const unsigned char *array, const size_t len)
{
    for (size_t i = 0; i < len; i++)
    {
        if (array[i] >= ' ' && array[i] <= '~')
        {
            uart0_putc(array[i]);
        }
        else
        {
            /* sprintf would be fine, but it bloats the firmware */
            uart0_puts_p(PSTR("\"0x"));
            print_char_hex(array[i]);
            uart0_putc('"');
        }
    }

    uart0_puts_p(crlf);
}


/* Print a single character's hexadecimal value */
void print_char_hex(const unsigned char c)
{
    uart0_putc((c >> 4) + ((c >> 4) <= 9 ? 0x30 : 0x37));
    uart0_putc((c & 0x0F) + ((c & 0x0F) <= 9 ? 0x30 : 0x37));
}
