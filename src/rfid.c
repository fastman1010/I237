#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include "hmi_msg.h"
#include "print_helper.h"
#include "rfid.h"
#include "andygock_avr-uart/uart.h"
#include "hd44780_111/hd44780.h"
#include "matejx_avr_lib/mfrc522.h"

card_t *firstCard = NULL;
time_t rfid_queries = 0;

/* Scan for presence of RFID tags, perform necessary actions if tag
 * is found. Door should be held open for 2 seconds, message shown for
 * 5 seconds. */
inline void rfid_scan(void)
{
    static time_t time_lastReading, time_lastMsg, now, rfid_queries_prev;
    static door_state_t door_state;
    static display_state_t display_state;
    static char display_needsUpdate;
    card_t *currentCard;
    static Uid uid, uid_prev; /* Card UID shall be stored here */
    char lcd_buffer[LCD_VISIBLE_COLS + 1] = {0x00};
    byte bufferATQA[2];
    byte bufferSize = sizeof(bufferATQA);
    now = time(NULL);

    if (rfid_queries_prev != rfid_queries) /* Time for a new query */
    {
        byte result = PICC_WakeupA(bufferATQA, &bufferSize);

        if (result == STATUS_OK || result == STATUS_COLLISION)
        {
            currentCard = firstCard;
            PICC_ReadCardSerial(&uid); /* Get card data */

            /* These actions are taken only if new card is given:
             * remember last UID, update last access (reading time),
             * tell that it is necessary to update data on display */
            if (memcmp(uid.uidByte, uid_prev.uidByte, UID_MAX_LEN) != 0)
            {
                uid_prev = uid;
                time_lastReading = now;
                display_needsUpdate = 1;
            }

            while (currentCard != NULL)
            {
                if (memcmp(uid.uidByte, currentCard->uid, currentCard->uidSize) == 0)
                {
                    /* UID match */
                    door_state = door_opening;
                    display_state = display_name;
                    break; /* Exit loop */
                }
                else
                {
                    currentCard = currentCard->nextCard;
                }
            }

            if (currentCard == NULL)
            {
                /* If we are here, the card was read, but no matches were
                 * found. Lock the door and forget the card */
                door_state = door_closing;
                display_state = display_access_denied;
            }

            PICC_HaltA(); /* Change card state to HALT */
        }

        /* Check state and time */
        switch (door_state)
        {
        case door_opening:
            PORTA |= _BV(LED_DOOR);
            door_state = door_open;
            break;

        case door_open:
            if ((now - time_lastReading) >= DOOR_OPEN_TIME)
            {
                memcpy(uid_prev.uidByte, "\0", UID_MAX_LEN);
                door_state = door_closing;
            }

            break;

        case door_closing:
            PORTA &= ~_BV(LED_DOOR);
            door_state = door_closed;
            break;

        case door_closed:
            break;

        default:
            break;
        }

        switch (display_state)
        {
        case display_name:
            if (currentCard->name != NULL)
                /* Truncate name if it is longer than LCD row */
            {
                strncpy(lcd_buffer, currentCard->name, LCD_VISIBLE_COLS);
            }
            else
            {
                strcpy_P(lcd_buffer, PSTR("Cannot read name"));
            }

            time_lastMsg = now;
            display_state = display_clear;
            break;

        case display_access_denied:
            strcpy_P(lcd_buffer, accessDenied);
            time_lastMsg = now;
            display_state = display_clear;
            break;

        case display_clear:
            if ((now - time_lastMsg) >= DISPLAY_UPDATE_TIME)
            {
                strcpy(lcd_buffer, "\0");
                display_needsUpdate = 1;
                display_state = display_no_update;
            }

            break;

        case display_no_update:
            break;

        default:
            break;
        }

        if (display_needsUpdate == 1)
        {
            lcd_clr(LCD_ROW_2_START, LCD_VISIBLE_COLS);
            lcd_goto(LCD_ROW_2_START);
            lcd_puts(lcd_buffer);
            display_needsUpdate = 0;
        }

        rfid_queries_prev = rfid_queries;
        /* PORTA ^= _BV(PORTA4); // Toggle blue LED for debugging */
    }
}

/* Read an RFID card and display its data */
void cli_rfid_read(const char *const *argv)
{
    (void) argv;
    Uid uid; /* Card UID shall be stored here */
    /* Prepare for card selection */
    byte bufferATQA[2];
    byte bufferSize = sizeof(bufferATQA);
    byte result = PICC_WakeupA(bufferATQA, &bufferSize);

    if (result == STATUS_OK || result == STATUS_COLLISION)
    {
        PICC_ReadCardSerial(&uid);
        uart0_puts_p(PSTR("Card selected!\r\n"));
        uart0_puts_p(PSTR("UID size: 0x"));
        print_char_hex(uid.size);
        uart0_puts_p(PSTR("\r\nCard type: "));
        uart0_puts(PICC_GetTypeName(PICC_GetType(uid.sak)));
        uart0_puts_p(PSTR("\r\nCard UID: "));

        for (byte i = 0; i < uid.size; i++)
        {
            print_char_hex(uid.uidByte[i]);
        }

        uart0_puts_p(crlf);
        PICC_HaltA();
    }
    else
    {
        uart0_puts_p(PSTR("Unable to select card.\r\n"));
    }
}


/* Print data about known RFID cards */
void cli_rfid_print(const char *const *argv)
{
    (void) argv;
    unsigned int i, cards;
    char buffer[4];
    card_t *currentCard;
    currentCard = firstCard;
    cards = 0;

    if (currentCard == NULL)
    {
        uart0_puts_p(PSTR("No cards on the list.\r\n"));
    }
    else
    {
        /* Loop until end of the list is reached */
        while (currentCard != NULL) /* Does the card exist? */
        {
            itoa(++cards, buffer, 10); /* Count cards, convert to string */
            uart0_puts(buffer);
            uart0_puts_p(PSTR(". Card UID["));
            itoa(currentCard->uidSize, buffer, 10); /* UID size to string */
            uart0_puts(buffer);
            uart0_puts_p(PSTR(" bytes]: "));

            /* Print UID */
            for (i = 0; i < currentCard->uidSize; i++)
            {
                print_char_hex(currentCard->uid[i]);
            }

            uart0_puts_p(PSTR(", owner name: "));
            uart0_puts(currentCard->name); /* Name */
            uart0_puts_p(crlf); /* Go to new line */
            currentCard = currentCard->nextCard; /* Go on to next card */
        }
    }
}


/* Add an RFID card to the list */
void cli_rfid_add(const char *const *argv)
{
    card_t *newCard, *currentCard, *lastCard;
    currentCard = firstCard;
    lastCard = firstCard;
    char buffer[4];
    /* Allocate memory to store card data */
    newCard = malloc(sizeof(card_t));

    /* malloc returns NULL if there is not enough memory.
     * Stop here. */
    if (newCard == NULL)
    {
        uart0_puts_p(out_of_memory);
        return;
    }

    if (uid_to_bytes(newCard, argv[1])) /* Read UID size and contents */
    {
        /* Go further if reading succeeded */
        /* Check if the card is already on the list */
        while (currentCard != NULL)
        {
            if (memcmp(newCard->uid, currentCard->uid, currentCard->uidSize) == 0)
            {
                /* memcmp returns zero if all bytes match. Size is taken
                 * from currentCard because known UID may be shorter than
                 * the new one, so assume that all cards, whose UID begins
                 * with this known combination of bytes, belong to the
                 * same owner. */
                uart0_puts_p(PSTR("Card already on the list.\r\n"));
                free(newCard);
                return;
            }
            else
            {
                lastCard = currentCard;
                currentCard = currentCard->nextCard;
            }
        }

        /* Card not on the list yet? That's a good sign. */
        newCard->name = malloc(strlen(argv[2]) + 1); /* Extra byte for \0 */

        if (newCard->name == NULL) /* Memory allocation failed? */
        {
            uart0_puts_p(out_of_memory);
            free(newCard);
            return;
        }

        strcpy(newCard->name, argv[2]); /* Copy name */
        uart0_puts_p(PSTR("Added card with UID: "));

        for (int posUid = 0; posUid < newCard->uidSize; posUid++)
        {
            print_char_hex(newCard->uid[posUid]);
        }

        itoa(newCard->uidSize, buffer, 10);
        uart0_puts_p(PSTR(" ("));
        uart0_puts(buffer);
        uart0_puts_p(PSTR(" bytes), owner name: "));
        uart0_puts(newCard->name);
        uart0_puts_p(crlf);
        newCard->nextCard = NULL;

        /* Structure ready. By this point currentCard should point to
         * NULL, so we take the last known card and replace its nextCard
         * pointer */
        if (firstCard == NULL)
        {
            firstCard = newCard;
        }
        else
        {
            lastCard->nextCard = newCard;
        }
    }
    else
    {
        uart0_puts_p(PSTR("Could not add a card.\r\n"));
        free(newCard);
    }
}


/* Remove an RFID card from the list */
void cli_rfid_rm(const char *const *argv)
{
    card_t tempCard;
    card_t *currentCard, *lastCard;
    currentCard = firstCard;
    lastCard = firstCard;

    if (firstCard == NULL) /* How do you kill that which has no life? */
    {
        uart0_puts_p(PSTR("No cards on the list, can't remove anything.\r\n"));
        return;
    }

    /* Temporarily store data */
    if (uid_to_bytes(&tempCard, argv[1])) /* Read UID size and contents */
    {
        /* Go further if reading succeeded */
        /* Check if the card is present on the list */
        while (currentCard != NULL)
        {
            if (memcmp(tempCard.uid, currentCard->uid, tempCard.uidSize) == 0)
            {
                /* memcmp returns zero if all bytes match. Size is taken
                 * from tempCard because known UID may be shorter than
                 * the one provided, and if a longer UID is given,
                 * then we shall remove a single card, not a group of them
                 * (short UID on the list may correspond to all cards
                 * whose UIDs begin with the same combination of bytes). */
                uart0_puts_p(PSTR("Removing card UID: "));

                for (int posUid = 0; posUid < currentCard->uidSize; posUid++)
                {
                    print_char_hex(currentCard->uid[posUid]);
                }

                uart0_puts_p(PSTR(", owner name: "));
                uart0_puts(currentCard->name);
                uart0_puts_p(crlf);
                free(currentCard->name); /* Bye-bye, username */

                /* Now carefully join the previous and the following entries */
                if (currentCard == firstCard)
                {
                    firstCard = currentCard->nextCard;
                }
                else
                {
                    lastCard->nextCard = currentCard->nextCard;
                }

                free(currentCard); /* Now we can erase the entry */
                return;
            }
            else
            {
                lastCard = currentCard;
                currentCard = currentCard->nextCard;
            }
        }

        /* Out of the loop - no match found */
        uart0_puts_p(PSTR("No matches found.\r\n"));
    }
    else
    {
        uart0_puts_p(PSTR("Could not remove the card.\r\n"));
    }
}


/* Convert given strings of hex characters into UID bytes and size,
 * store them in the card_t structure at the given pointer.
 * Returns positive on success or 0 on failure */
unsigned char uid_to_bytes(card_t *card, const char *arg)
{
    size_t posArg, posUid;
    size_t len = strlen(arg); /* Get string length */

    /* Check string length; UID longer than 10 bytes not accepted */
    if (len > 2 * UID_MAX_LEN)
    {
        uart0_puts_p(PSTR("UID too long.\r\n"));
        return 0;
    }

    /* Check whether the input is hexadecimal */
    for (posArg = 0; posArg < len; posArg++)
    {
        if (!isxdigit(arg[posArg]))
        {
            uart0_puts_p(PSTR("Invalid UID provided (not hex).\r\n"));
            return 0;
        }
    }

    /* If we are here, the string can be worked with */
    posArg = 0; /* Current position in arg[] */
    posUid = 0; /* Current position in uid[] */
    card->uidSize = len / 2;

    /* Check length parity. If odd, first byte is 0x0n, otherwise 0xnn */
    if (len & 0x01)
    {
        card->uidSize++; /* Allocate extra byte */
        card->uid[posUid++] = hexchar_to_value(arg[posArg++]);
    }

    for (; posArg < len; posUid++, posArg++)
    {
        card->uid[posUid] = hexchar_to_value(arg[posArg++]) << 4;
        card->uid[posUid] |= hexchar_to_value(arg[posArg]);
    }

    return 1;
}


/* Convert a hex character (0-f) into its value.
 * If non-hexadecimal character is given, 0 is returned */
unsigned char hexchar_to_value(unsigned char c)
{
    if (c >= '0' && c <= '9')
    {
        c -= 0x30;
    }
    else if (c >= 'A' && c <= 'F')
    {
        c -= 0x37;
    }
    else if (c >= 'a' && c <= 'f')
    {
        c -= 0x57;
    }
    else
    {
        c = 0;
    }

    return c;
}
