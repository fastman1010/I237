#ifndef RFID_H
#define RFID_H

#include <time.h>

#define DISPLAY_UPDATE_TIME 5 /* seconds */
#define DOOR_OPEN_TIME 2 /* seconds */
#define LED_DOOR PORTA2 /* Arduino Mega digital pin 24 */
#define UID_MAX_LEN 10

typedef struct card
{
    unsigned char uidSize;
    unsigned char uid[UID_MAX_LEN];
    char *name;
    struct card *nextCard;
} card_t;

typedef	enum
{
    door_closed,
    door_closing,
    door_open,
    door_opening
} door_state_t;

typedef enum
{
    display_no_update,
    display_name,
    display_access_denied,
    display_clear
} display_state_t;

card_t *firstCard;
time_t rfid_queries;
extern void rfid_scan(void);
void cli_rfid_read(const char *const *argv);
void cli_rfid_print(const char *const *argv);
void cli_rfid_add(const char *const *argv);
void cli_rfid_rm(const char *const *argv);
unsigned char uid_to_bytes(card_t *card, const char *arg);
unsigned char hexchar_to_value(unsigned char c);

#endif /* RFID_H */
